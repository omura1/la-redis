﻿using RedisForm.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RedisForm
{
    public partial class RedisForm : Form
    {
        PersonRepository psrepo { get; set; }
        DepartmentRepository dprepo { get; set; }
        SQLRepository sqlrepo { get; set; }

        bool isMemcached =false;
        public RedisForm()
        {
            InitializeComponent();
             psrepo = new PersonRepository();
             dprepo = new DepartmentRepository();
            sqlrepo = new SQLRepository();
        }

        private void btn_kaydet_Click(object sender, EventArgs e)
        {
            Person person = new Person();
            
            List<Person> persons = new List<Person>();
            
            person.Name = txt_name.Text;

            person.Level = Convert.ToInt32(txt_level.Text);

            person.Department = dprepo.create(cmb_department.Text,"Yet Another Department");
             
            persons.Add(person);
             
            var res = psrepo.insert(persons);
            isMemcached = true;
            foreach (var item in res)
            {

                MessageBox.Show(item.PersonId+" , "+item.Name.ToString()+" , "+item.Level+" , "+item.Department.Name);
            }
        }

        private void btn_DiskeYaz_Click(object sender, EventArgs e)
        {
            if (isMemcached)
            {
                psrepo.diskeyaz();
                MessageBox.Show("Yazıldı!");
                isMemcached = false;
            }
            else
            {
                MessageBox.Show("Önce Cache de yazınız!");
            }
            
        }

  
        private void richTxt_persons_TextChanged(object sender, EventArgs e)
        {

        }

        private void RedisForm_Load(object sender, EventArgs e)
        {
              
        }

        private void cmb_FromTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show(cmb_FromTable.Text +" Seçildi! " );

            var allOfRecords = sqlrepo.SelectAll();
            if (allOfRecords == null || allOfRecords.Count() <= 0)
            {
                MessageBox.Show("Boştur!");
            }
            else if (cmb_FromTable.Text == "Department = IT")
            {

                allOfRecords = sqlrepo.SelectDepartment("IT");
            }
            else if (cmb_FromTable.Text == "Department = FINANCE")
            {
                allOfRecords = sqlrepo.SelectDepartment("Finance");
            }
            else if (cmb_FromTable.Text == "LEVEL>500")
            {
                allOfRecords = sqlrepo.SelectLevel(500);
            }

            richTxt_persons.AppendText("---------List of "+ cmb_FromTable.Text + "----------------------\n");
            foreach (var itPerson in allOfRecords)
            {
                richTxt_persons.AppendText(itPerson.ToString()+"\n");
            }
            richTxt_persons.AppendText("-----------------End----------------------\n");
        }
    }
}
