﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisForm
{
    public class BaseRepository
    {
        public RedisClient client { get; set; }
        public BaseRepository() {
             client = new RedisClient("127.0.0.1", 6379);
            
        }
    }
}
