﻿using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisForm.Repository
{
    class SQLRepository : BaseRepository
    {
        IRedisTypedClient<Person> personStore { get; set; }
        IOrderedEnumerable<Person> selectedPersons_ { get; set; }
        public SQLRepository():base(){
            personStore = client.As<Person>();
        }

        public IOrderedEnumerable<Person> SelectAll()
        { 
            selectedPersons_ = personStore
               .GetAll() 
               .OrderByDescending(p => p.Level); 
            return selectedPersons_;
        }

        public IOrderedEnumerable<Person> SelectDepartment(String departmentName_) {

             selectedPersons_ = personStore
                .GetAll()
               .Where<Person>(p => p.Department.Name == departmentName_)
                .OrderByDescending(p => p.Level);
             
            return selectedPersons_;
        }

        public IOrderedEnumerable<Person> SelectLevel(int levelDegree_)
        {

             selectedPersons_ = personStore
                .GetAll()
               .Where<Person>(p => p.Level > levelDegree_)
                .OrderByDescending(p => p.Level); 

            return selectedPersons_;
        }
    }
}
