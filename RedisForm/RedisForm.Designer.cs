﻿namespace RedisForm
{
    partial class RedisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_kaydet = new System.Windows.Forms.Button();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_level = new System.Windows.Forms.TextBox();
            this.cmb_department = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.richTxt_persons = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmb_FromTable = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_kaydet
            // 
            this.btn_kaydet.Location = new System.Drawing.Point(128, 162);
            this.btn_kaydet.Name = "btn_kaydet";
            this.btn_kaydet.Size = new System.Drawing.Size(121, 23);
            this.btn_kaydet.TabIndex = 0;
            this.btn_kaydet.Text = "KAYDET";
            this.btn_kaydet.UseVisualStyleBackColor = true;
            this.btn_kaydet.Click += new System.EventHandler(this.btn_kaydet_Click);
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(128, 36);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(121, 20);
            this.txt_name.TabIndex = 1;
            // 
            // txt_level
            // 
            this.txt_level.Location = new System.Drawing.Point(128, 80);
            this.txt_level.Name = "txt_level";
            this.txt_level.Size = new System.Drawing.Size(121, 20);
            this.txt_level.TabIndex = 2;
            // 
            // cmb_department
            // 
            this.cmb_department.FormattingEnabled = true;
            this.cmb_department.Items.AddRange(new object[] {
            "IT",
            "FINANCE"});
            this.cmb_department.Location = new System.Drawing.Point(128, 124);
            this.cmb_department.Name = "cmb_department";
            this.cmb_department.Size = new System.Drawing.Size(121, 21);
            this.cmb_department.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Level";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Department";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(48, 354);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(331, 45);
            this.button1.TabIndex = 8;
            this.button1.Text = "Diske Yaz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btn_DiskeYaz_Click);
            // 
            // richTxt_persons
            // 
            this.richTxt_persons.Location = new System.Drawing.Point(48, 241);
            this.richTxt_persons.Name = "richTxt_persons";
            this.richTxt_persons.Size = new System.Drawing.Size(331, 107);
            this.richTxt_persons.TabIndex = 10;
            this.richTxt_persons.Text = "";
            this.richTxt_persons.TextChanged += new System.EventHandler(this.richTxt_persons_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Select  *";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "from  Person  Where";
            // 
            // cmb_FromTable
            // 
            this.cmb_FromTable.FormattingEnabled = true;
            this.cmb_FromTable.Items.AddRange(new object[] {
            "Department = IT",
            "Department = FINANCE",
            "LEVEL>500"});
            this.cmb_FromTable.Location = new System.Drawing.Point(208, 214);
            this.cmb_FromTable.Name = "cmb_FromTable";
            this.cmb_FromTable.Size = new System.Drawing.Size(171, 21);
            this.cmb_FromTable.TabIndex = 12;
            this.cmb_FromTable.SelectedIndexChanged += new System.EventHandler(this.cmb_FromTable_SelectedIndexChanged);
            // 
            // RedisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 443);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmb_FromTable);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.richTxt_persons);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmb_department);
            this.Controls.Add(this.txt_level);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.btn_kaydet);
            this.Name = "RedisForm";
            this.Text = "RediisForm";
            this.Load += new System.EventHandler(this.RedisForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_kaydet;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.TextBox txt_level;
        private System.Windows.Forms.ComboBox cmb_department;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTxt_persons;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmb_FromTable;
    }
}

